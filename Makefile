.PHONY: clean version build appinspect rundocker

APP_NAME := splunk_traefik_addon
APP_ROOT := src
APP_PATH := $(APP_ROOT)/$(APP_NAME)
VERSION := 0.$(CI_PIPELINE_IID).0
APP_PACKAGE_NAME := $(APP_NAME)_$(VERSION).tgz
COLON := :

version: ## Update the apps' versions
	sed -i -e "s/SPLUNK_VERSION_REPLACE/${VERSION}/" -e "s/BUILD_SHORT_HASH/${VERSION}/" $(APP_PATH)/default/app.conf && \
	sed -i -e "s/SPLUNK_VERSION_REPLACE/${VERSION}/" -e "s/BUILD_SHORT_HASH/${VERSION}/" $(APP_PATH)/app.manifest
	

build: clean version ## Build tar balls containing the apps
	mkdir -p ./build
	tar -czvf ./build/$(APP_PACKAGE_NAME) -C $(APP_ROOT) $(APP_NAME)
	mkdir -p out
	cp build/*.tgz out/$(PACKAGE_NAME)

appinspect: clean version
	mkdir -p ./out
	cp -a src/ ./out
	export APP_PATH=out
	splunk-appinspect inspect $(APP_PATH) --output-file out/appinspect_result.json --included-tags splunk_appinspect --included-tags cloud --excluded-tags manual --included-tags appapproval --excluded-tags prerelease --mode precert

install:
	uv venv
	. .venv/bin/activate
	uv pip install -r requirements.txt

rundocker:
	docker-compose -f genlog/docker-compose.yml up -d

clean:
	rm -rf ./build || true
	rm -rf ./out || true
	chmod -R u+rw,g-rwx,o-rwx $(APP_PATH)
